# Tootin-Pride

A small CSS snippet that adapts the "Publish!" button's background color to show pride.

<p align="center">
    <img src="img/preview.png" style="border-radius: 4px" alt="A small preview of the CSS in action!">
</p>

## Table of contents

1. [Getting started](#getting-started)
    - [Mastodon](#mastodon)
2. [Editions](#editions)
3. [Support](#support)

<br>

## Getting started
### Mastodon
- If you are an admin of an instance and want to "install" the style instance wide, use the `*.css` files and copy the whole content into the "[Custom CSS](https://fedi.tips/customising-your-mastodon-servers-appearance)" field. (The files are linked in the [Editions section](#editions))
    - Please at least put a comment with a link to this repo into your custom CSS before the code. Any further mentions and acknowledgement is much appreciated.
- If you just want to use the style locally in your browser (only desktop browsers), take a look at their respective https://userstyles.world page. (If available, they are linked in the [Editions section](#editions))

## Editions

| Standard | Simple | 
| - | - |
| <img src="img/preview.png" width="500px" style="border-radius: 4px" alt="A small preview of the standard edition"><div align="center"><p>Authors: vndmtrx & MagicLike</p><p>➡️ <a href="https://github.com/vndmtrx/mastodon-pride-button">Source</a></p><p>📂 <a href="https://codeberg.org/MagicLike/Tootin-Pride/src/branch/main/tootin-pride.css">CSS file</a> · 🌐 <a href="https://userstyles.world/style/10147/tootin-pride">UserStyles.world</a></p><a href="https://userstyles.world/api/style/10147.user.css"><img src="https://img.shields.io/badge/Install%20directly%20with-Stylus-116b59?style=flat"></a></div> | <img src="img/preview_simple.png" width="500px" style="border-radius: 4px" alt="A small preview of the simple edition"><div align="center"><p>Authors: MagicLike & vndmtrx</p><p>📂 <a href="https://codeberg.org/MagicLike/Tootin-Pride/src/branch/main/editions/tootin-pride_simple.css">CSS file</a></p></div> |

Others will be added soon! (- if I get your permission ❤️)

## Support

If you like the work you see here, please consider supporting the work and effort of the Authors by making a donation.

- Support MagicLike:
    - https://magiclike.net/#donate
- Support vndmtrx:
    - They wish that if you want to support them, you should better send your money to a nonprofit!